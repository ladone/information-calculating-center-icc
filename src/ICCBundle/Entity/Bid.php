<?php
/**
 * Created by PhpStorm.
 * User: Ladone
 * Date: 07.07.2016
 * Time: 16:00
 */

namespace ICCBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="bid")
 */
class Bid
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $date;

    /**
     * @ORM\Column(type="time", nullable=false)
     */
    private $time;


    /**
     * @ORM\Column(type="string", length=30, nullable=false)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=30, nullable=false)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=30, nullable=false)
     */
    private $patronymic;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $post;

    /**
     * @ORM\Column(type="string", length=23, nullable=false)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $housing;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $audience;

    /**
     * @ORM\Column(type="string", length=1000, nullable=false)
     */
    private $task;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $headLaboratory;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="ICCBundle\Entity\StorageHistoryEntry", mappedBy="bid")
     */
    private $storageHistoryEntries;

    /**
     * @ORM\OneToMany(targetEntity="ICCBundle\Entity\StorageHistoryEntryItemDate", mappedBy="bid")
     */
    private $storageHistoryEntryItemDate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->storageHistoryEntries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return bid
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return bid
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return bid
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return bid
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set patronymic
     *
     * @param string $patronymic
     * @return bid
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    /**
     * Get patronymic
     *
     * @return string 
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * Set post
     *
     * @param string $post
     * @return bid
     */
    public function setPost($post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return string 
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return bid
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set housing
     *
     * @param string $housing
     * @return bid
     */
    public function setHousing($housing)
    {
        $this->housing = $housing;

        return $this;
    }

    /**
     * Get housing
     *
     * @return string 
     */
    public function getHousing()
    {
        return $this->housing;
    }

    /**
     * Set audience
     *
     * @param string $audience
     * @return bid
     */
    public function setAudience($audience)
    {
        $this->audience = $audience;

        return $this;
    }

    /**
     * Get audience
     *
     * @return string 
     */
    public function getAudience()
    {
        return $this->audience;
    }

    /**
     * Set task
     *
     * @param string $task
     * @return bid
     */
    public function setTask($task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return string 
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set headLaboratory
     *
     * @param string $headLaboratory
     * @return bid
     */
    public function setHeadLaboratory($headLaboratory)
    {
        $this->headLaboratory = $headLaboratory;

        return $this;
    }

    /**
     * Get headLaboratory
     *
     * @return string 
     */
    public function getHeadLaboratory()
    {
        return $this->headLaboratory;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return bid
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add storageHistoryEntries
     *
     * @param \ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntries
     * @return bid
     */
    public function addStorageHistoryEntry(\ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntries)
    {
        $this->storageHistoryEntries[] = $storageHistoryEntries;

        return $this;
    }

    /**
     * Remove storageHistoryEntries
     *
     * @param \ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntries
     */
    public function removeStorageHistoryEntry(\ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntries)
    {
        $this->storageHistoryEntries->removeElement($storageHistoryEntries);
    }

    /**
     * Get storageHistoryEntries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStorageHistoryEntries()
    {
        return $this->storageHistoryEntries;
    }

    public function __toString(){
        return (string)$this->getId();
    }

    /**
     * Add storageHistoryEntryItemDate
     *
     * @param \ICCBundle\Entity\StorageHistoryEntryItemDate $storageHistoryEntryItemDate
     * @return Bid
     */
    public function addStorageHistoryEntryItemDate(\ICCBundle\Entity\StorageHistoryEntryItemDate $storageHistoryEntryItemDate)
    {
        $this->storageHistoryEntryItemDate[] = $storageHistoryEntryItemDate;

        return $this;
    }

    /**
     * Remove storageHistoryEntryItemDate
     *
     * @param \ICCBundle\Entity\StorageHistoryEntryItemDate $storageHistoryEntryItemDate
     */
    public function removeStorageHistoryEntryItemDate(\ICCBundle\Entity\StorageHistoryEntryItemDate $storageHistoryEntryItemDate)
    {
        $this->storageHistoryEntryItemDate->removeElement($storageHistoryEntryItemDate);
    }

    /**
     * Get storageHistoryEntryItemDate
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStorageHistoryEntryItemDate()
    {
        return $this->storageHistoryEntryItemDate;
    }
}
