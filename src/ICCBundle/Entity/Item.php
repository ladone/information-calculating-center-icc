<?php
/**
 * Created by PhpStorm.
 * User: Ladone
 * Date: 21.11.2016
 * Time: 15:04
 */

namespace ICCBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="items")
 */
class Item
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ICCBundle\Entity\Storage", inversedBy="items")
     * @ORM\JoinColumn(name="storage_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $storage;

    /**
     * @ORM\Column(type="string", length=90, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $count;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $summ;

    /**
     * @ORM\Column( type="integer")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="ICCBundle\Entity\StorageHistoryEntry", mappedBy="item")
     */
    private $storageHistoryEntries;

    public function __toString()
    {
        return (string)$this->getId();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->storageHistory = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set count
     *
     * @param integer $count
     * @return item
     */
    public function setCount($count)
    {
        $this->count = $count;
        $this->summ = $this->getPrice() * $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return item
     */
    public function setPrice($price)
    {
        $this->price = $price;
        $this->summ = $this->getCount() * $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set summ
     *
     * @param string $summ
     * @return item
     */
    public function setSumm($summ)
    {
        $this->summ = $summ;

        return $this;
    }

    /**
     * Get summ
     *
     * @return string 
     */
    public function getSumm()
    {
        return $this->summ;
    }

    /**
     * Set storage
     *
     * @param \ICCBundle\Entity\Storage $storage
     * @return item
     */
    public function setStorage(\ICCBundle\Entity\Storage $storage)
    {
        $this->storage = $storage;

        return $this;
    }

    /**
     * Get storage
     *
     * @return \ICCBundle\Entity\Storage 
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * Add storageHistoryEntries
     *
     * @param \ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntries
     * @return item
     */
    public function addStorageHistoryEntry(\ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntries)
    {
        $this->storageHistoryEntries[] = $storageHistoryEntries;

        return $this;
    }

    /**
     * Remove storageHistoryEntries
     *
     * @param \ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntries
     */
    public function removeStorageHistoryEntry(\ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntries)
    {
        $this->storageHistoryEntries->removeElement($storageHistoryEntries);
    }

    /**
     * Get storageHistoryEntries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStorageHistoryEntries()
    {
        return $this->storageHistoryEntries;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return item
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }
}
