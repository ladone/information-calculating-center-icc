<?php

namespace ICCBundle\Controller;

use Doctrine\ORM\Query;
use ICCBundle\Entity\StorageHistoryEntryItemDate;
use ICCBundle\Entity\WriteOffHistoryEntry;
use ICCBundle\Libraries\MoneyToStr;
use ICCBundle\Libraries\IntToString;
use ICCBundle\Entity\Item;
use ICCBundle\Entity\Storage;
use ICCBundle\Entity\StorageHistoryEntry;
use ICCBundle\Entity\User;
use ICCBundle\Entity\WriteOffDate;
use ICCBundle\Form\Type\ItemChoiceType;
use ICCBundle\Form\Type\UserEditType;
use ICCBundle\Form\Type\UserSettingsType;
use MyProject\Proxies\__CG__\stdClass;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ICCBundle\Entity\Bid;
use ICCBundle\Form\FormModel\BidFormModel;
use ICCBundle\Form\Type\ItemType;
use ICCBundle\Form\Type\DeleteType;
use ICCBundle\Form\Type\BidType;
use ICCBundle\Form\Type\BidEditType;
use Symfony\Component\Templating\Tests\Storage\StorageTest;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineODMPhpcrAdapter;
use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Component\Validator\Constraints\Date;
//use mpdf\mPDF;

class DefaultController extends Controller
{
    /**
     * Главная страница
     * @Route("/", name="index")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        return [

        ];
        // replace this example code with whatever you need
       /* return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));*/
    }

    /**
     * Страница администратора
     * @Route("/admin/", name="admin")
     * @Template()
     */
    public function adminAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $repoStorages = $em->getRepository('ICCBundle:Storage');
        $repoStorages = $repoStorages->findAll();

        return [
            'repoStorages' => $repoStorages
        ];
    }

    /**
     * Страница пользователя
     * @Route("/admin/users/", name="user")
     * @Template()
     */
    public function userAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repoUser = $em->getRepository('ICCBundle:User');
        $deleteType = $this->createForm(DeleteType::class);
        $repoUser = $repoUser->findAll();

        return [
            'repoUser' => $repoUser,
            'deleteType' => $deleteType->createView()
        ];
    }

    /**
     * Изменение пользователя
     * @Route("/admin/users/{id}", requirements={"id" = "\d+"}, defaults={"id" = 0}, name="edit_user")
     * @Template()
     * @Method({"POST"})
     */
    public function userEditAction(Request $request, $id)
    {
        $container = $this->container;
        $role = $container->getParameter('security.role_hierarchy.roles');
        $userManager = $this->container->get('fos_user.user_manager');
        $userManager->findUserBy(array('id'=>$id));


        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(User::class);
        $user = $repo->find($id);
        $formEditUser= $this->createForm(UserEditType::class, $user);
        if (!$user) {
            throw $this->createNotFoundException('Reserve not found.');
        }
        $userPass = $user->getPassword();
        $formEditUser->handleRequest($request);
        if($formEditUser->isValid()){
            $userManager->updatePassword($user);
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('user');
        }

        $em->flush();
        return [
            'formEditUser' => $formEditUser->createView(),
        ];
    }

    /**
     * Удалеие пользователя
     * @Route("/admin/users/delete/{id}", requirements={"id" = "\d+"}, defaults={"id" = 0}, name="user_delete")
     * @Template()
     * @Method({"POST"})
     */
    public function userDeleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(User::class);

        $user = $repo->find($id);
        if (!$user) {
            throw $this->createNotFoundException('Reserve not found.');
        }

        $deleteForm = $this->createForm(DeleteType::class);
        $deleteForm->handleRequest($request);
        $em->remove($user);
        $em->flush();
        return $this->redirectToRoute('user');
    }

    /**
     * Страница заявок
     * @Route("/admin/bid/", name="bid")
     * @Template()
     */
    public function bidAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $page = $request->query->getInt('page', 1);

        $deleteType = $this->createForm(DeleteType::class);
        $repBid = $em->getRepository(Bid::class);
        $repItems = $em->getRepository('ICCBundle:Item');
        $items = $repItems->findBy(['storage' => '1']);
        $bid = $repBid->findAll();

        $queryBuilder = $repBid->createQueryBuilder('bid')->orderBy('bid.date', 'DESC')->addOrderBy('bid.time', 'DESC');


        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($queryBuilder));
        $pagerfanta->setCurrentPage($page);

        return [
            'bid' => $bid,
            'deleteType' => $deleteType->createView(),
            'pagerfanta' => $pagerfanta,
            'items'  => $items,
        ];
    }

    /**
     * Создание заявки
     * @Route("/admin/bid/create/", name="new_bid")
     * @Template()
     */
    public function bidCreateAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $bid = new Bid();
        $newBid = $this->createForm(BidType::class, $bid);
        $bid->setStatus(false);


        $repBid = $this->getDoctrine()->getRepository('ICCBundle:Bid');

        $newBid->handleRequest($request);
        if ($newBid->isValid()) {
            $em->persist($bid);
            $em->flush();
            return $this->redirectToRoute('print_bid', array('id' => $bid->getId()));
        }

        return [
            'newBid' => $newBid->createView(),
        ];
    }

    /**
     * Завершение заявки
     * @Route("/admin/bid/complete/", name="complete_bid")
     * @Template()
     */
    public function bidCompleteAction(Request $request){
        $em          = $this->getDoctrine()->getManager(); // Менеджер сущностей
        $user        = $this->container->get('security.context')->getToken()->getUser(); // Менеджер юзера
        $repoUser    = $em->getRepository(User::class);       // Получение списка пользователей
        $repoBid     = $em->getRepository(Bid::class);         // Получение списка заявок
        $repoStroage = $em->getRepository(Storage::class); // Получение объект склада
        $repoItem    = $em->getRepository(Item::class);       // Получение спаска материалов


        $user = $repoUser->find($user);     // Поиск пользователя


        $items = $request->request->get('item'); // Получение POST запроса от собственной формы
        $getItems = []; // Инициализированый массив для хранения полученых материалов

        $bidId = $items['DefaultValues']['bidId']; // Индификатор заявки

        $SHEID = $em->getRepository(StorageHistoryEntryItemDate::class)->findOneBy(['bid' =>$bidId]);

//        dump($SHEID);
        $bid = $repoBid->find($bidId);  // Поиск заявки по индификатору


        $storage = $repoStroage->find(1); // ВНИМАНИЕ!!! Поиск скалада.
                                          // По умолчанию id 1 = «Інформаційно обчислювальний центр»

        // Блок проверки полученых материалов с запроса
        if (isset($items['Items'])){
            foreach ($items['Items'] as $key=>$item){ // Перебор массива $items для дальнейшей его обработки
                $getItems[$key] = $item;
            }

            foreach  ($getItems as $items){  // Чательная обработка массива $items
                $storageHistoryEntry = new StorageHistoryEntry(); // Новий экземпляр объекта

                /// Перебор массива $items из преобразованого массива $getItems
                foreach ($items as $key=>$value){
                    if(!empty($value)){
                       $itemID = $items['itemId'];

                        $storageHistoryEntry->setBid($bid);
                        $storageHistoryEntry->setStorage($storage);
                        $item = $repoItem->find($itemID);
                        $storageHistoryEntry->setItem($item);

                        if($key == 'count'){
                            $item = $repoItem->find($itemID);

                            if($item->getCount() >= $value){
                                $storageHistoryEntry->setCount($value);
                                $item->setCount($item->getCount() - $value);
                            }else {
                                echo 'Склад не має стільки запасів'.'<br/>'.$item->getName().': '.$item->getCount();
                            }

                        }

                        $dateTime = new \DateTime(); // Обьект даты
//                        dump($SHEID);
                        if($SHEID == null)
                        {
                            $storageHistoryEntry->setDate($dateTime); // Присвоение даты
                            $storageHistoryEntry->setTime($dateTime); // Присвоение времени
                        }
                        else
                        {
                            $storageHistoryEntry->setDate($SHEID->getDate()); // Присвоение даты
                            $storageHistoryEntry->setTime($SHEID->getTime()); // Присвоение времени
                        }

                        $storageHistoryEntry->setUserLogin($user->getUsername()); // запись учетной записи пользователя
                        $bid->setStatus(true); // Установление флага что заявка выполнена
                        $em->persist($storageHistoryEntry); // Передача объекта менеджеру сущностей.

                    }
                    else {
                        echo 'Не корректні данні';
                        return $this->redirectToRoute('bid');
                    }
                }
            }

            // Удалить дату списания.
            if($SHEID != null)
                $em->remove($SHEID);


            $em->flush();
        }
        else {
            $bid->setStatus(true);
            $em->persist($bid);
            $em->flush();
        }


        if (!$bid) {
            throw $this->createNotFoundException('Bid not found.');
        }


        return $this->redirectToRoute('bid');

//        $em->persist($bid);

    }

    /**
     * Изменение заявки
     * @Route("/admin/bid/edit/{id}", requirements={"id" = "\d+"}, defaults={"id" = 0}, name="edit_bid")
     * @Template()
     */
    public function bidEditAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Bid::class);
        $bid = $repo->find($id);

        $formEditBid = $this->createForm(BidEditType::class, $bid);
        if (!$bid) {
            throw $this->createNotFoundException('Bid not found.');
        }

        $formEditBid->handleRequest($request);
        if($formEditBid->isValid()){

            // get form data
            $formData = $formEditBid->getData();
//            dump($formData->getStatus());

//            dump($formData);
            if($formData->getStatus() == 0)
            {
                // repository Storage History Entry
                $repoSHE = $em->getRepository(StorageHistoryEntry::class);
                $SHE     = $repoSHE->findBy(['bid' => $formData->getId()]);
                $SHEID   = new StorageHistoryEntryItemDate();


                if(!empty($SHE))
                {
                    $WOHE = $em->getRepository(WriteOffHistoryEntry::class)->findBy(['storageHistoryEntry' => $SHE[0]->getId()]);

                    // handle all entries update count and sum
                    foreach($SHE as $entry)
                    {
                        $entry->getItem()->setCount($entry->getCount()+$entry->getItem()->getCount());
                        $em->persist($entry);
                    }

                    $SHEID->setBid($bid);
                    $SHEID->setDate($SHE[0]->getDate());
                    $SHEID->setTime($SHE[0]->getTime());
                    $em->persist($SHEID);

                    // remove write off history entry
                    foreach($WOHE as $entry)
                    {
                        $em->remove($entry);
                    }

                    // remove all entries
                    foreach($SHE as $entry)
                    {
                        $entry->setWriteOffStatus(0);
                        $entry->setWriteOffDate(NULL);
                        $entry->setWriteOffTime(NULL);
                        $em->remove($entry);
                    }

                }

            }
//            dump($SHE[0]->getItem()->getSumm());


            $em->persist($bid);
            $em->flush();
            return $this->redirectToRoute('bid');
        }

        return [
            'formEditBid' => $formEditBid->createView(),
            'bid' => $bid,
        ];
    }

    /**
     * Печать заявки
     * @Route("/admin/bid/print/{id}", requirements={"id" = "\d+"}, defaults={"id" = 0}, name="print_bid")
     * @Template()
     */
    public function bidPrintAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Bid::class);
		$twig = $this->get('twig');

        $bid = $repo->find($id);
        if (!$bid) {
            throw $this->createNotFoundException('Bid not found.');
        }

		$html = $twig->render('@ICC/Default/bidPrint.html.twig', array(
			'bid' => $bid,
		));

		$mpdf = $this->get('tfox.mpdfport')->getMpdf([10,'dejavuserif']);
		$mpdf->SetTitle('Заявка');
		$mpdf->WriteHTML($html);

		// Output a PDF file directly to the browser
		$mpdf->Output('bid_'.$bid->getId(), 'I');

        return [];
    }

    /**
     * Удаление заявки
     * @Route("/admin/bid/delete/{id}", requirements={"id" = "\d+"}, defaults={"id" = 0}, name="admin_bid_delete")
     * @Template()
     * @Method({"POST"})
     */
    public function bidDeleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Bid::class);

        $bid = $repo->find($id);
        if (!$bid) {
            throw $this->createNotFoundException('Reserve not found.');
        }

        $deleteForm = $this->createForm(DeleteType::class);
        $deleteForm->handleRequest($request);
        $em->remove($bid);
        $em->flush();
        return $this->redirectToRoute('bid');
    }

    /**
     * @Route("/admin/installationCertificate/", name="installation_certificate")
     * @Template()
     */
    public function installationCertificateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager(); // get entities manager
        $connection = $em->getConnection(); // connection for SQL querirs


        $ic = $request->query->get('ic');


        // dump($ic);


        // SQL query
        $sql = "";

        $smtp = $connection->prepare("
                                        SELECT she.item_id, i.name, i.price, she.count as count, bid.housing, bid.audience
                                        FROM storageshistoryentry as she 
                                        INNER JOIN items as i 
                                            ON she.item_id = i.id
                                        AND i.`type` = 1
                                        AND i.storage_id = :storage
                                        AND she.date BETWEEN :first AND :second
                                        LEFT JOIN bid
                                            ON she.bid_id = bid.id
                                    ");
        $smtp->bindValue('storage', $ic['storage']);
        $smtp->bindValue('first', date('Y-m-d', strtotime($ic['period']['first'])));
        $smtp->bindValue('second', date('Y-m-d', strtotime($ic['period']['second'])));
        $smtp->execute();
        $result = $smtp->fetchAll();

        // Получение репозитория склада
        $repoStorages = $em->getRepository('ICCBundle:Storage');
        $storages = $repoStorages->findAll();

        return [
            'ic' => $ic,
            'result' => $result,
            'storages' => $storages,
        ];
    }

    /**
     * Генерация страницы сертификатов акта установки
     * @Route("/admin/installationCertificate/generate/", name="installation_certificate_generate")
     */
    public function installationCertificateGenerateAction(Request $request){
        $em = $this->getDoctrine()->getManager(); // get entities manager
        $connection = $em->getConnection(); // connection for SQL querirs
        $twig = $this->get('twig');

        $print = $request->request->get('print');
        $storageId = $print['storage'];
        $repoStorage = $em->getRepository('ICCBundle:Storage');
        $print['storage'] = $repoStorage->find($storageId)->getName();

        $date['month'] = substr($print['second'], 3, 2);
        $date['year'] = substr($print['second'], 6, 4);

        $months = [1 => 'січень', 2 => 'лютий', 3 =>'березень',
                   4 => 'квітень', 5 => 'травень', 6 => 'червень',
                   7 => 'липень', 8 => 'серпень', 9 => 'вересень',
	               10 => 'жовтень', 11 => 'листопад', 12 => 'грудень'];

        foreach($months as $key => $month){
            if($date['month'] == $key){
                $date['month'] = $month;
            }
        }


        // SQL query
        $sql = "";

        $smtp = $connection->prepare("
                                        SELECT she.item_id, i.name, i.price, she.count as count, bid.housing, bid.audience
                                        FROM storageshistoryentry as she 
                                        INNER JOIN items as i 
                                            ON she.item_id = i.id
                                        AND i.`type` = 1
                                        AND i.storage_id = :storage
                                        AND she.date BETWEEN :first AND :second
                                        LEFT JOIN bid
                                            ON she.bid_id = bid.id
                                    ");
        $smtp->bindValue('storage', $storageId);
        $smtp->bindValue('first', date('Y-m-d', strtotime($print['first'])));
        $smtp->bindValue('second', date('Y-m-d', strtotime($print['second'])));
        $smtp->execute();
        $result = $smtp->fetchAll();



        $html = $twig->render('@ICC/Default/installationCertificateGenerate.html.twig', array(
                                 'result' => $result,
                                 'print' => $print,
                                 'date' => $date,
                             ));

        $mpdf = $this->get('tfox.mpdfport')->getMpdf([12,'dejavuserif']);

        $mpdf->WriteHTML($html);

        // Output a PDF file directly to the browser
        $mpdf->Output('instalation_certificate_'.date('H-i_d-m-Y'), 'I');

        return [];
    }

    /**
     * @Route("/admin/writeoff/", name="writeOffAct")
     * @Template();
     */
    public function writeOffAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();

        // Обьявление репозиториев
        $repoSHE = $em->getRepository('ICCBundle:StorageHistoryEntry');
        $repoStorages = $em->getRepository('ICCBundle:Storage');

        $qb = $repoSHE->createQueryBuilder('she');
        $writeOff = $request->query->get('writeOff');
        // dump($writeOff);
        // dump($request->get('writeoff'));

        // Получение складов
        $storages = $repoStorages->findAll();

        $connection = $em->getConnection(); // connection for SQL querirs
        $smtp = $connection->prepare("
                                         SELECT she.item_id, i.name, i.type, i.price, she.count as count, bid.housing, bid.audience
                                         FROM storageshistoryentry as she 
                                         INNER JOIN items as i 
                                          ON she.item_id = i.id
                                           AND i.storage_id = :storage
                                           AND she.date BETWEEN :first AND :second
                                           AND she.write_off_status = 0
                                         LEFT JOIN bid
                                          ON she.bid_id = bid.id
                                    ");
        $smtp->bindValue('storage', $request->get('writeOff')['storage']);
        $smtp->bindValue('first', date('Y-m-d', strtotime($request->get('writeOff')['first'])));
        $smtp->bindValue('second', date('Y-m-d', strtotime($request->get('writeOff')['second'])));
        $smtp->execute();
        $result = $smtp->fetchAll();
//        dump($result);

        return [
            'result' => $result,
            'storages' => $storages,
            'writeOff' => $writeOff,
        ];

    }

    /**
     * @Route("/admin/writeoff/generate/", name="writeOffActGenerate")
     */
    public function writeOffGenerateAction(Request $request)
    {
        // get entity manager
        $em = $this->getDoctrine()->getEntityManager();

        // new object
        $money2str = new MoneyToStr('UAH', 'UKR', "TEXT");

        // New entities
        $writeOff = new WriteOffDate();


        // POST data
        $writeOffData = $request->request->get('print');


        // create query builder
        $qb = $em->createQueryBuilder()
            ->select('she')
            ->from('ICCBundle:StorageHistoryEntry', 'she')
            ->where('she.date BETWEEN :first AND :second')
            ->setParameter('first', date('Y-m-d', strtotime($writeOffData['first'])))
            ->setParameter('second', date('Y-m-d', strtotime($writeOffData['second'])))
            ->andWhere('she.writeOffStatus = 0')
            ->getQuery()
            ->getResult();


        $updateTable = $em->createQueryBuilder();

        // get ids from SHE
        $idsSHE = [];


        if(!empty($qb))
        {


            $writeOff->setFirstDate(new \DateTime($writeOffData['first']));
            $writeOff->setSecondDate(new \DateTime($writeOffData['second']));

            $em->persist($writeOff);
            $em->flush();


            foreach ($qb as $item)
            {
                array_push($idsSHE, $item->getId());

                $wohe = new WriteOffHistoryEntry();

                $wohe->setStorageHistoryEntry($item);
                $wohe->setWriteOffDate($writeOff);
                $em->persist($wohe);
            }
            $em->flush();



            $updateTable
                ->update('ICCBundle:StorageHistoryEntry', 'she')
                ->set('she.writeOffStatus', '1')
                ->set('she.writeOffDate', date('ymd'))
                ->set('she.writeOffTime', date('His'))
                ->where('she.id IN (:ids)')
                ->setParameter('ids', $idsSHE)
                ->getQuery()
                ->getResult();

        }


        $summ = 0;
        $countElements = 0;

        foreach ($qb as $key => $item) {
            $summ += $item->getItem()->getPrice() * $item->getCount();
            $countElements++;
        }

        $twig = $this->get('twig');

        $html = $twig->render('@ICC/Default/writeOffDocGenerate.html.twig', [
            'qb' => $qb,
            'summString' => $money2str->convertValue($summ),
            'summ' => $summ,
            'countElements' => $countElements,
            'countElementsString' => $money2str->triad2Word($countElements, 0, "M"),
        ]);

        $mpdf = $this->get('tfox.mpdfport')->getMpdf([10,'dejavuserif']);
        $mpdf->WriteHTML($html);



        // Output a PDF file directly to the browser
        $mpdf->Output('write_off_'.date('H-i_d-m-Y').'.pdf', 'I');



        //https://github.com/javadev/moneytostr-russian/blob/master/src/main/php/MoneyToStr.php


        return [];
    }

    /**
     * Склад
     * @Route("/admin/storages/{id}", requirements={"id" = "\d+"}, defaults={"id" = 0}, name="storages")
     * @Template()
     */
    public function storagesAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();

        // Индификатор склада
        $deleteType = $this->createForm(DeleteType::class);


        $repoStorages = $em->getRepository('ICCBundle:Storage');

        $repoStorages = $repoStorages->find($id);

        $repoItem = $em->getRepository('ICCBundle:Item');
        $repoItem = $repoItem->findBy(['storage' => $id]);

        // Запрос количества суммы товаров на складе
        $storageSumm = $em->createQueryBuilder()
            ->select('item.summ')
            ->from('ICCBundle:Item', 'item')
            ->where('item.storage = '.$id)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);

        $summItem = 0;

        foreach ($storageSumm AS $key=>$itemSumm){
            $summItem += $itemSumm['summ'];
        }


        $items = new Item();

        $formItem = $this->createForm(ItemType::class, $items);

        $formItem->handleRequest($request);
        if($formItem->isValid()){
            $items->setStorage($repoStorages);
            $em->persist($items);
            $em->flush();
            return $this->redirectToRoute('storages', ['id' => $id]);
        }

        return [
            'deleteType'    => $deleteType->createView(),
            'storages_id'   => $id,
            'formItem'      => $formItem->createView(),
            'repoStorages'  => $repoStorages,
            'repoItem'      => $repoItem,
            'summItem'      => $summItem
        ];
    }

    /**
     * Удаление позиции со склада
     *
     * @Route("/admin/storages/delete/{storages_id}/{id}", requirements={"storages_id" = "\d+", "id" = "\d+"}, defaults={"storages_id" = 0, "id" = 0}, name="storage_item_delete")
     * @Template()
     * @Method({"POST"})
     */
    public function storageItemDeleteAction(Request $request, $storages_id, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Item::class);

        $item = $repo->findOneBy(['storage'=>$storages_id, 'id' => $id]);
        if (!$item) {
            throw $this->createNotFoundException('Reserve not found.');
        }

        $deleteForm = $this->createForm(DeleteType::class);
        $deleteForm->handleRequest($request);
        $em->remove($item);
        $em->flush();
        return $this->redirectToRoute('storages', ['id' => $storages_id]);
    }

    /**
     * Изменение скалада
     *
     * @Route("/admin/storages/edit/{storages_id}/{id}", requirements={"storages_id" = "\d+", "id" = "\d+"}, defaults={"storages_id" = 0, "id" = 0}, name="storage_edit")
     * @Template()
     */
    public function  storageEditAction(Request $request, $storages_id, $id) {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Item::class);
        $item = $repo->findOneBy(['storage'=>$storages_id, 'id' => $id]);;
        $formEditItem = $this->createForm(ItemType::class, $item);
        if (!$item) {
            throw $this->createNotFoundException('Storage not found.');
        }

        $formEditItem->handleRequest($request);
        if($formEditItem->isValid()){
            $em->persist($item);
            $em->flush();
            return $this->redirectToRoute('storages', ['id' => $storages_id]);
        }

        return [
            'formEditItem' => $formEditItem->createView(),
            'item' => $item,
            'storage_id' => $storages_id,
        ];
    }

    /**
     * История списания со склада
     * @Route("/admin/storages/history/", name="storage_history")
     * @Template()
     */
    public function storageHistoryEntriesAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $page = $request->query->getInt('page', 1);

        $repo = $em->getRepository(StorageHistoryEntry::class);
        //$itemsEntries = $repo->findBy(['storage' => 1]);

        $queryBuilder = $repo->createQueryBuilder('storageHistoryEntry')->orderBy('storageHistoryEntry.date', 'DESC')
            ->addOrderBy('storageHistoryEntry.time', 'DESC');

        $storageSumm = $em->createQueryBuilder()
            ->select('storageHistoryEntry.summ')
            ->from('ICCBundle:StorageHistoryEntry', 'storageHistoryEntry')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);



        $summItem = 0;

        foreach ($storageSumm AS $key=>$itemSumm){
            $summItem += $itemSumm['summ'];
        }

       /* $itemsEntries = $em->createQueryBuilder()         // Создание запроса для
            ->select('e')                                   // удобного предсталвения данных
            ->from('ICCBundle:StorageHistoryEntry', 'e')    // с одинаковым ID
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);*/

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($queryBuilder));
        $pagerfanta->setCurrentPage($page);

        return [
            'itemEntries' => $pagerfanta,
            'summItem'   => $summItem,
        ];
    }



    /**
     * Настройки юзера
     *
     * @Route("/settings/", name="user_settings")
     * @Template
     */
    public function settingsAction(Request $request){
        $user = $user = $this->container->get('security.context')->getToken()->getUser();

        $userManager = $this->container->get('fos_user.user_manager');
        $userManager->findUserBy(array('id'=>$user));

        $em = $this->getDoctrine()->getEntityManager();
        $repo = $em->getRepository(User::class);

        $user = $repo->find($user);
        $formEditUser = $this->createForm(UserSettingsType::class, $user);

        $userPass = $user->getPassword();

        $formEditUser->handleRequest($request);
        if($formEditUser->isValid()){
            $userManager->updatePassword($user);
            $em->persist($user);
            $em->flush();
        }


        return [
            'formEditUser' => $formEditUser->createView(),
        ];

    }


    /**
     * Пісочниця
     * @Route("/admin/sandbox/", name="sandbox")
     * @Template()
     */
    public function sandboxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager(); // get entities manager
        $connection = $em->getConnection(); // connection for SQL querirs

        $inttostring = new IntToString();

        $inttostring->convert(129);

        $m = new MoneyToStr('UAH', 'UKR', 'TEXT');

        echo $m->triad2Word(0, 0, "M");
        return [];
    }






}
