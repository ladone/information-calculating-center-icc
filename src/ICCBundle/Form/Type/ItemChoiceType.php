<?php

namespace ICCBundle\Form\Type;

use Proxies\__CG__\ICCBundle\Entity\Storage;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use ICCBundle\Entity\Items;

class ItemChoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            /** @var Items $items */
            $items = $event->getData();
            $form = $event->getForm();
            $itemChoice = [];

            foreach ($items as $item){
                $itemChoice[$item->getName()] = $item->getid();
            }
            
            $form->add('name', ChoiceType::class, [
                'label' => false,
                'choices' => $itemChoice,
                'choices_as_values' => true,
            ])->add('count', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Кількість'
                ]
            ]);
        });
    }

   /* public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Items::class
        ));
    }*/

}