<?php
/**
 * Created by PhpStorm.
 * User: ladone
 * Date: 6/22/17
 * Time: 2:25 PM
 */

namespace ICCBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="write_off_history_entries")
 */
class WriteOffHistoryEntry
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ICCBundle\Entity\StorageHistoryEntry", inversedBy="writeOffHistoryEntry")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $storageHistoryEntry;


    /**
     * @ORM\ManyToOne(targetEntity="ICCBundle\Entity\WriteOffDate", inversedBy="writeOffHistoryEntry", cascade={"remove"})
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $writeOffDate;
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set storageHistoryEntry
     *
     * @param \ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntry
     * @return WriteOffHistoryEntry
     */
    public function setStorageHistoryEntry(\ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntry = null)
    {
        $this->storageHistoryEntry = $storageHistoryEntry;

        return $this;
    }

    /**
     * Get storageHistoryEntry
     *
     * @return \ICCBundle\Entity\StorageHistoryEntry 
     */
    public function getStorageHistoryEntry()
    {
        return $this->storageHistoryEntry;
    }

    /**
     * Set writeOffDate
     *
     * @param \ICCBundle\Entity\WriteOffDate $writeOffDate
     * @return WriteOffHistoryEntry
     */
    public function setWriteOffDate(\ICCBundle\Entity\WriteOffDate $writeOffDate = null)
    {
        $this->writeOffDate = $writeOffDate;

        return $this;
    }

    /**
     * Get writeOffDate
     *
     * @return \ICCBundle\Entity\WriteOffDate 
     */
    public function getWriteOffDate()
    {
        return $this->writeOffDate;
    }
}
