<?php
/**
 * Created by PhpStorm.
 * User: Ladone
 * Date: 05.07.2016
 * Time: 12:27
 */

namespace ICCBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use ICCBundle\Entity\Item;


class ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
               'label' => false,
               'attr' => array(
                   'autocomplete' => 'off',
                   'placeholder' => 'Найменування'
               ),
            ))
            ->add('count', NumberType::class, array(
                'label' => false,
                'attr' => array(
                    'autocomplete' => 'off',
                    'placeholder' => 'Кількість',
                ),
            ))
            ->add('price', MoneyType::class, array(
                'label' => false,
                'currency' => false,
                'scale' => 2,
                'currency' => 'UAH',
                'attr' => array(
                    'autocomplete' => 'off',
                    'placeholder' => 'Ціна'
                ),
            ))
            ->add('type', ChoiceType::class, array(
                'label' => false,
                'choices' => array(
                    'Росходний матеріал' => 0,
                    'Запасна частина' => 1,
                ),
                'choices_as_values' => true,
                'attr' => array(
                    'autocomplete' => 'off',
                    'placeholder' => 'Тип',
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Item::class,
        ));
    }

}