<?php
/**
 * Created by PhpStorm.
 * User: Ladone
 * Date: 27.07.2017
 * Time: 16:08
 */

namespace ICCBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="storage_history_entry_item_date")
 */
class StorageHistoryEntryItemDate
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ICCBundle\Entity\Bid", inversedBy="storageHistoryEntryItemDate")
     * @ORM\JoinColumn(name="bid_id", referencedColumnName="id")
     */
    private $bid;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="time")
     */
    private $time;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return StorageHistoryEntryItemDate
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return StorageHistoryEntryItemDate
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set bid
     *
     * @param \ICCBundle\Entity\Bid $bid
     * @return StorageHistoryEntryItemDate
     */
    public function setBid(\ICCBundle\Entity\Bid $bid = null)
    {
        $this->bid = $bid;

        return $this;
    }

    /**
     * Get bid
     *
     * @return \ICCBundle\Entity\Bid 
     */
    public function getBid()
    {
        return $this->bid;
    }
}
