<?php
/**
 * Created by PhpStorm.
 * User: Ladone
 * Date: 21.11.2016
 * Time: 14:58
 */

namespace ICCBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="storages")
 */
class Storage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60, nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="ICCBundle\Entity\Item", mappedBy="storage")
     */
    private $items;

    /**
     * @ORM\OneToMany(targetEntity="ICCBundle\Entity\StorageHistoryEntry", mappedBy="storage")
     */
    private $storageHistoryEntries;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
        $this->storageHistoryEntries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Storage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add items
     *
     * @param \ICCBundle\Entity\Item $items
     * @return Storage
     */
    public function addItem(\ICCBundle\Entity\Item $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \ICCBundle\Entity\Item $items
     */
    public function removeItem(\ICCBundle\Entity\Item $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Add storageHistoryEntries
     *
     * @param \ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntries
     * @return Storage
     */
    public function addStorageHistoryEntry(\ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntries)
    {
        $this->storageHistoryEntries[] = $storageHistoryEntries;

        return $this;
    }

    /**
     * Remove storageHistoryEntries
     *
     * @param \ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntries
     */
    public function removeStorageHistoryEntry(\ICCBundle\Entity\StorageHistoryEntry $storageHistoryEntries)
    {
        $this->storageHistoryEntries->removeElement($storageHistoryEntries);
    }

    /**
     * Get storageHistoryEntries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStorageHistoryEntries()
    {
        return $this->storageHistoryEntries;
    }
}
