<?php
/**
 * Created by PhpStorm.
 * User: ladone
 * Date: 6/21/17
 * Time: 2:07 PM
 */

namespace ICCBundle\Controller;
use Doctrine\ORM\Query;
use ICCBundle\Entity\WriteOffDate;
use ICCBundle\Entity\WriteOffHistoryEntry;
use ICCBundle\Libraries\MoneyToStr;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class WriteOffController extends Controller
{
    /**
     * @Route("/admin/writeOff/list/", name="write_off_list")
     * @Template()
     */
    public function writeOffListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $repoWOD = $em->getRepository(WriteOffDate::class)->findAll();

        return [
            'writeOffs' => $repoWOD,
        ];
    }

    /**
     * @Route("/admin/writeOff/list/generate/{id}", requirements={"id" = "\d+"}, defaults={"id" = 0}, name="write_off_list_print")
     * @Method("POST")
     */
    public function writeOffListGenerateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $repoWOD = $em->getRepository(WriteOffDate::class);
        $repoWOHE = $em->getRepository(WriteOffHistoryEntry::class);

        $WOD = $repoWOD->find($id);
        $WOHE = $repoWOHE->findBy(['writeOffDate' => $id]);

        // new object
        $money2str = new MoneyToStr('UAH', 'UKR', "TEXT");


        if (!$WOD) {
            throw $this->createNotFoundException('Reserve not found.');
        }

        if (!$WOHE) {
            throw $this->createNotFoundException('Reserve not found.');
        }

        // Не получается получить данные.
        // create query builder
        /*$qb = $em->createQueryBuilder()
            ->select('she')
            ->from('ICCBundle:StorageHistoryEntry', 'she')
            ->where('she.date BETWEEN :first AND :second')
            ->setParameter('first', "2017-03-05")
            ->setParameter('second',"2017-03-05")
            ->andWhere('she.writeOffStatus = 1')
            ->getQuery()
            ->getResult();*/

        $qb = [];

        foreach ($WOHE as $item)
        {
            $qb[] = $item->getStorageHistoryEntry();
        }

        // get ids from SHE
        $idsSHE = [];


        if(!empty($qb))
        {
            foreach ($qb as $item)
            {
                array_push($idsSHE, $item->getId());

            }
        }


        $summ = 0;
        $countElements = 0;

        foreach ($qb as $key => $item) {
            $summ += $item->getItem()->getPrice() * $item->getCount();
            $countElements++;
        }

        $twig = $this->get('twig');

        $html = $twig->render('@ICC/Default/writeOffDocGenerate.html.twig', [
            'qb' => $qb,
            'summString' => $money2str->convertValue($summ),
            'summ' => $summ,
            'countElements' => $countElements,
            'countElementsString' => $money2str->triad2Word($countElements, 0, "M"),
        ]);

        $mpdf = $this->get('tfox.mpdfport')->getMpdf([10,'dejavuserif']);
        $mpdf->WriteHTML($html);



        // Output a PDF file directly to the browser
        $mpdf->Output('write_off_'.date('H-i_d-m-Y').'.pdf', 'I');





        return [];

    }

    /**
     * @Route("/admin/writeOff/list/delete/{id}", requirements={"id" = "\d+"}, defaults={"id" = 0}, name="write_off_list_delete")
     * @Method("POST")
     */
    public function writeOffDeleteAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $repoWOD = $em->getRepository(WriteOffDate::class);
        $repoWOHE = $em->getRepository(WriteOffHistoryEntry::class);

        $WOD = $repoWOD->find($id);
        $WOHE = $repoWOHE->findBy(['writeOffDate' => $id]);


        if (!$WOD) {
            throw $this->createNotFoundException('Reserve not found.');
        }

        if (!$WOHE) {
            throw $this->createNotFoundException('Reserve not found.');
        }

        // update to nullable items from StorageHistoryEntry
        foreach ($WOHE as $item)
        {
            $item->getStorageHistoryEntry()->setWriteOffStatus(0);
            $item->getStorageHistoryEntry()->setWriteOffDate(null);
            $item->getStorageHistoryEntry()->setWriteOffTime(null);

            $em->persist($item);
        }

        // remove WOHE
        foreach($WOHE as $item)
        {
            $em->remove($item);
        }

        // remove WOD
        $em->remove($WOD);

        $em->flush();

        return $this->redirectToRoute('write_off_list');
    }

}