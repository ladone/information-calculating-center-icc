<?php
/**
 * Created by PhpStorm.
 * User: Ladone
 * Date: 19.07.2016
 * Time: 10:19
 */

namespace ICCBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;

class UserEditType extends AbstractType
{
    private $roleHierarchy;

    public function __construct(array $roleHierarchy)
    {
        $this->roleHierarchy = $roleHierarchy;
    }

    public function changeRole($roleHierarchy){
        $roles = [];
        foreach ($roleHierarchy as $key=>$role){
            $roles[$key] = $key;
        }

        return $roles;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('lastname', TextType::class, array(
                'label' => 'Прізвище',
                'attr' => array(
                    'autocomplete' => false,
                    'placeholder' => 'Прізвище'
                ),
            ))
            ->add('firstname', TextType::class, array(
                'label' => 'Ім\'я',
                'attr' => array(
                    'autocomplete' => false,
                    'placeholder' => 'Ім\'я',
                ),
            ))
            ->add('patronymic', TextType::class, array(
            'label' => 'По-батькові',
                'attr' => array(
                    'autocomplete' => false,
                    'placeholder' => 'По-батькові'
                ),
            ))
            ->add('email', TextType::class, array(
            'label' => 'Електронна пошта',
                'attr' => array(
                    'autocomplete' => false,
                    'placeholder' => 'Електронна пошта'
                ),
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'required' => false,
                'first_options'  => array('label' => 'Пароль'),
                'second_options' => array('label' => 'Повторіть пароль'),
            ))
            ->add('roles', ChoiceType::class, array(
                'multiple' => true,
                'choices' => $this->changeRole($this->roleHierarchy),
            ));
    }


    public function configureOptions(OptionsResolver $resolver)
    {
       /* $resolver->setDefaults(array(
            'data_class' => User::class,
        ));*/
    }
}