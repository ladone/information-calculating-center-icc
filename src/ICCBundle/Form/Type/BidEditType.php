<?php
/**
 * Created by PhpStorm.
 * User: Ladone
 * Date: 14.07.2016
 * Time: 16:08
 */

namespace ICCBundle\Form\Type;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use ICCBundle\Entity\Bid;


class BidEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('date', DateType::class, array(
                'label' => 'Дата',
                'choice_translation_domain' => true,
                'widget' => 'single_text',
                'format' => 'dd - MM - yyyy',
                'attr' => array(
                    'autocomplete' => 'off',
                    'placeholder' => 'Дата',
                ),
            ))
            ->add('time', TimeType::class, array(
                'label' => 'Час',
                'widget' => 'single_text',
                'attr' => array(
                    'autocomplete' => 'off',
                    'placeholder' => 'Час',
                ),
            ))
            ->add('lastname', TextType::class, array(
                'label' => 'Прізвище',
                'attr' => array(
                    'autocomplete' => 'off',
                    'placeholder' => 'Прізвище',
                ),

            ))
            ->add('firstname', TextType::class, array(
                'label' => 'Ім\'я',
                'attr' => array(
                    'autocomplete' => 'off',
                    'placeholder' => 'Ім\'я',
                ),

            ))
            ->add('patronymic', TextType::class, array(
                'label' => 'По батькові',
                'attr' => array(
                    'autocomplete' => 'off',
                    'placeholder' => 'По батькові',
                ),
            ))
            ->add('post', TextType::class, array(
                'label' => 'Посада',
                'attr' => array(
                    'autocomplete' => 'off',
                    'placeholder' => 'Посада',
                ),
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Контактний телефон',
                'attr' => array(
                    'autocomplete' => 'off',
                    'placeholder' => 'Контактний телефон',
                ),
            ))
            ->add('housing', ChoiceType::class, array(
                'label' => 'Корпус',
                'choices' => [
                    'Корпуси' => [
                        'Головний' => 'Головний',
                        'Гуманітарний' => 'Гуманітарний',
                        'ОТД' => 'ОТД',
                        'Худ. Граф.' => 'Худ. Граф.',
                        'Муз. Пед.' => 'Муз. Пед.',
                    ],
                    'Гуртожитки' => [
                        'Гуртожиток №1' => 'Гуртожиток №1',
                        'Гуртожиток №2' => 'Гуртожиток №2',
                        'Гуртожиток №3' => 'Гуртожиток №3',
                    ]
                ]
            ))
            ->add('audience', TextType::class, array(
                'label' => 'Аудиторія',
                'attr' => array(
                    'autocomplete' => 'off',
                    'placeholder' => 'Аудиторія',
                ),
            ))
            ->add('task', TextareaType::class, array(
                'label' => 'Завдання / ознаки несправності КТ',
                'attr' => array(
                    'autocomplete' => 'off',
                    'placeholder' => 'Завдання / ознаки несправності КТ',
                ),
            ))
            ->add('headLaboratory', TextType::class, array(
                'label' => 'Начальник ІОЦ',
                'attr' => array(
                    'placeholder' => 'Начальник ІОЦ',
                ),
            ))
            ->add('status', ChoiceType::class, array(
                'label' => 'Статус виконання',
                'choices_as_values' => true,
                'choices' => array(
                   'Не виконано' => 0,
                   'Виконано' => 1
                ),
            ));
    }

}