<?php
/**
 * Created by PhpStorm.
 * User: Ladone
 * Date: 26.11.2016
 * Time: 15:55
 */

namespace ICCBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="storageshistoryentry")
 */
class StorageHistoryEntry
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ICCBundle\Entity\Bid", inversedBy="storageHistoryEntries")
     * @ORM\JoinColumn(name="bid_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $bid;

    /**
     * @ORM\ManyToOne(targetEntity="ICCBundle\Entity\Storage", inversedBy="storageHistoryEntries")
     * @ORM\JoinColumn(name="storage_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $storage;

    /**
     * @ORM\ManyToOne(targetEntity="ICCBundle\Entity\Item", inversedBy="storageHistoryEntries")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $item;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="time")
     */
    private $time;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $count;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $summ;

    /**
     * @ORM\Column(type="string", length=70, nullable=false)
     */
    private $userLogin;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $writeOffStatus = 0;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $writeOffDate;

     /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $writeOffTime;

    /**
     * @ORM\OneToMany(targetEntity="ICCBundle\Entity\WriteOffHistoryEntry", mappedBy="storageHistoryEntry")
     */
    private $writeOffHistoryEntry;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->writeOffHistoryEntry = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return StorageHistoryEntry
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return StorageHistoryEntry
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set count
     *
     * @param string $count
     * @return StorageHistoryEntry
     */
    public function setCount($count)
    {
        $this->count = $count;
        $this->summ = $this->getItem()->getPrice() * $count;
        return $this;
    }

    /**
     * Get count
     *
     * @return string 
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set summ
     *
     * @param string $summ
     * @return StorageHistoryEntry
     */
    public function setSumm($summ)
    {
        $this->summ = $summ;

        return $this;
    }

    /**
     * Get summ
     *
     * @return string 
     */
    public function getSumm()
    {
        return $this->summ;
    }

    /**
     * Set userLogin
     *
     * @param string $userLogin
     * @return StorageHistoryEntry
     */
    public function setUserLogin($userLogin)
    {
        $this->userLogin = $userLogin;

        return $this;
    }

    /**
     * Get userLogin
     *
     * @return string 
     */
    public function getUserLogin()
    {
        return $this->userLogin;
    }

    /**
     * Set writeOffStatus
     *
     * @param boolean $writeOffStatus
     * @return StorageHistoryEntry
     */
    public function setWriteOffStatus($writeOffStatus)
    {
        $this->writeOffStatus = $writeOffStatus;

        return $this;
    }

    /**
     * Get writeOffStatus
     *
     * @return boolean 
     */
    public function getWriteOffStatus()
    {
        return $this->writeOffStatus;
    }

    /**
     * Set writeOffDate
     *
     * @param \DateTime $writeOffDate
     * @return StorageHistoryEntry
     */
    public function setWriteOffDate($writeOffDate)
    {
        $this->writeOffDate = $writeOffDate;

        return $this;
    }

    /**
     * Get writeOffDate
     *
     * @return \DateTime 
     */
    public function getWriteOffDate()
    {
        return $this->writeOffDate;
    }

    /**
     * Set writeOffTime
     *
     * @param \DateTime $writeOffTime
     * @return StorageHistoryEntry
     */
    public function setWriteOffTime($writeOffTime)
    {
        $this->writeOffTime = $writeOffTime;

        return $this;
    }

    /**
     * Get writeOffTime
     *
     * @return \DateTime 
     */
    public function getWriteOffTime()
    {
        return $this->writeOffTime;
    }

    /**
     * Set bid
     *
     * @param \ICCBundle\Entity\Bid $bid
     * @return StorageHistoryEntry
     */
    public function setBid(\ICCBundle\Entity\Bid $bid)
    {
        $this->bid = $bid;

        return $this;
    }

    /**
     * Get bid
     *
     * @return \ICCBundle\Entity\Bid 
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set storage
     *
     * @param \ICCBundle\Entity\Storage $storage
     * @return StorageHistoryEntry
     */
    public function setStorage(\ICCBundle\Entity\Storage $storage)
    {
        $this->storage = $storage;

        return $this;
    }

    /**
     * Get storage
     *
     * @return \ICCBundle\Entity\Storage 
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * Set item
     *
     * @param \ICCBundle\Entity\Item $item
     * @return StorageHistoryEntry
     */
    public function setItem(\ICCBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \ICCBundle\Entity\Item 
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Add writeOffHistoryEntry
     *
     * @param \ICCBundle\Entity\WriteOffHistoryEntry $writeOffHistoryEntry
     * @return StorageHistoryEntry
     */
    public function addWriteOffHistoryEntry(\ICCBundle\Entity\WriteOffHistoryEntry $writeOffHistoryEntry)
    {
        $this->writeOffHistoryEntry[] = $writeOffHistoryEntry;

        return $this;
    }

    /**
     * Remove writeOffHistoryEntry
     *
     * @param \ICCBundle\Entity\WriteOffHistoryEntry $writeOffHistoryEntry
     */
    public function removeWriteOffHistoryEntry(\ICCBundle\Entity\WriteOffHistoryEntry $writeOffHistoryEntry)
    {
        $this->writeOffHistoryEntry->removeElement($writeOffHistoryEntry);
    }

    /**
     * Get writeOffHistoryEntry
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWriteOffHistoryEntry()
    {
        return $this->writeOffHistoryEntry;
    }
}
