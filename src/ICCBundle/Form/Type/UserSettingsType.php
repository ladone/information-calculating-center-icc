<?php
/**
 * Created by PhpStorm.
 * User: Ladone
 * Date: 01.08.2016
 * Time: 14:26
 */
namespace ICCBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use ICCBundle\Entity\User;
use Symfony\Component\Validator\Constraints\Choice;

class UserSettingsType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('lastname', TextType::class, array(
                'label' => 'Прізвище',
                'attr' => array(
                    'autocomplete' => false,
                    'placeholder' => 'Прізвище'
                ),
            ))
            ->add('firstname', TextType::class, array(
                'label' => 'Ім\'я',
                'attr' => array(
                    'autocomplete' => false,
                    'placeholder' => 'Ім\'я',
                ),
            ))
            ->add('patronymic', TextType::class, array(
                'label' => 'По-батькові',
                'attr' => array(
                    'autocomplete' => false,
                    'placeholder' => 'По-батькові'
                ),
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'required' => false,
                'first_options'  => array('label' => 'Пароль'),
                'second_options' => array('label' => 'Повторіть пароль'),
            ));
    }
}