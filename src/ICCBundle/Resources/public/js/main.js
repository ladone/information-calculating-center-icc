$(document).ready(function(){

    $('#bid_edit_date, #bid_date').data("DateTimePicker");

    $('#bid_edit_date, #bid_date').datetimepicker({
        format: 'DD - MM - YYYY',
        locale: 'uk'
    });

    $('#bid_edit_time, #bid_time').datetimepicker({
        format: 'HH:mm',
        locale: 'uk'
    });
});