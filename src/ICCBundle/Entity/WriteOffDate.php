<?php
/**
 * Created by PhpStorm.
 * User: ladone
 * Date: 6/21/17
 * Time: 3:41 PM
 */

namespace ICCBundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="write_off_date")
 */
class WriteOffDate
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $firstDate;

    /**
     * @ORM\Column(type="date")
     */
    private $secondDate;

    /**
     * @ORM\OneToMany(targetEntity="ICCBundle\Entity\WriteOffHistoryEntry", mappedBy="writeOffDate")
     */
    private $writeOffHistoryEntry;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->writeOffHistoryEntry = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstDate
     *
     * @param \DateTime $firstDate
     * @return WriteOffDate
     */
    public function setFirstDate($firstDate)
    {
        $this->firstDate = $firstDate;

        return $this;
    }

    /**
     * Get firstDate
     *
     * @return \DateTime 
     */
    public function getFirstDate()
    {
        return $this->firstDate;
    }

    /**
     * Set secondDate
     *
     * @param \DateTime $secondDate
     * @return WriteOffDate
     */
    public function setSecondDate($secondDate)
    {
        $this->secondDate = $secondDate;

        return $this;
    }

    /**
     * Get secondDate
     *
     * @return \DateTime 
     */
    public function getSecondDate()
    {
        return $this->secondDate;
    }

    /**
     * Add writeOffHistoryEntry
     *
     * @param \ICCBundle\Entity\WriteOffHistoryEntry $writeOffHistoryEntry
     * @return WriteOffDate
     */
    public function addWriteOffHistoryEntry(\ICCBundle\Entity\WriteOffHistoryEntry $writeOffHistoryEntry)
    {
        $this->writeOffHistoryEntry[] = $writeOffHistoryEntry;

        return $this;
    }

    /**
     * Remove writeOffHistoryEntry
     *
     * @param \ICCBundle\Entity\WriteOffHistoryEntry $writeOffHistoryEntry
     */
    public function removeWriteOffHistoryEntry(\ICCBundle\Entity\WriteOffHistoryEntry $writeOffHistoryEntry)
    {
        $this->writeOffHistoryEntry->removeElement($writeOffHistoryEntry);
    }

    /**
     * Get writeOffHistoryEntry
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWriteOffHistoryEntry()
    {
        return $this->writeOffHistoryEntry;
    }
}
