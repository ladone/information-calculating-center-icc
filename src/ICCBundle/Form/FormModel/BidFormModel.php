<?php
/**
 * Created by PhpStorm.
 * User: Ladone
 * Date: 11.07.2016
 * Time: 15:49
 */

namespace ICCBundle\Form\FormModel;


class BidFormModel
{

    private $id;
    private $date;
    private $time;
    private $firstname;
    private $lastname;
    private $patronymic;
    private $post;
    private $phone;
    private $housing;
    private $audience;
    private $task;
    private $headLaboratory;

    public function getId()
    {
        return $this->id;
    }

    public function setNumberBid($numberBid)
    {
        $this->numberBid = $numberBid;

        return $this;
    }

    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setHousing($housing)
    {
        $this->housing = $housing;

        return $this;
    }

    public function getHousing()
    {
        return $this->housing;
    }

    public function setAudience($audience)
    {
        $this->audience = $audience;

        return $this;
    }

    public function getAudience()
    {
        return $this->audience;
    }


    public function setHeadLaboratory($headLaboratory)
    {
        $this->headLaboratory = $headLaboratory;

        return $this;
    }

    public function getHeadLaboratory()
    {
        return $this->headLaboratory;
    }

    public function setTask($task)
    {
        $this->task = $task;

        return $this;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    public function getPatronymic()
    {
        return $this->patronymic;
    }

    public function setPost($post)
    {
        $this->post = $post;

        return $this;
    }

    public function getPost()
    {
        return $this->post;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }
}